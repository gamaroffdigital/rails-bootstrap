gem_group :development, :test do
  gem 'rspec-rails'
end

if yes?("Facebook app? (y/n)")
  gem 'koala'
end

if yes?("Use Stylus? (y/n)")
  gem 'stylus', '~> 1.0.0.beta'
  run 'rm app/assets/stylesheets/application.css'
  run 'echo "//= require_self\n\n@import \'nib\'\n\nbody\n  background: #f09" > app/assets/stylesheets/application.css.styl'
else
  gem 'bourbon'
  run 'rm app/assets/stylesheets/application.css'
  run 'echo "//= require_self\n\n@import \'bourbon\'\n\nbody\n  background: #f09" > app/assets/stylesheets/application.css.sass'
  application 'config.sass.preferred_syntax = :sass'
end

run 'bundle install'

run 'rm app/assets/javascripts/application.js'
file 'app/assets/javascripts/application.js.coffee', <<-CODE
#= require jquery
#= require jquery_ujs
#= require turbolinks
#= require_tree .
CODE

db_username = db_password = db_name = nil

while db_username.nil? || db_username.strip == '' || db_password.nil? || db_name.nil? || db_name.strip == '' do
  if db_username.nil? || db_username.strip == ''
    db_username = ask("What is your local PostgreSQL install username? If using Postgres.app, this will be #{`whoami`.strip} by default.")
  end

  if db_password.nil?
    db_password = ask("What is your local PostgreSQL install password? If using Postgres.app, this will be blank by default.")
  end

  if db_name.nil? || db_name.strip == ''
    db_name     = ask("What do you want to call your DB (e.g. my_app)? We'll append _development and _test to this to differentiate between environments.")
  end
end

file 'config/database.yml', <<-CODE
common: &common
  adapter: postgresql
  host: localhost
  username: #{db_username}
  password: #{db_password}
  encoding: unicode

development:
  <<: *common
  database: #{db_name}_development

test:
  <<: *common
  database: #{db_name}_test
CODE

rake 'db:drop'
rake 'db:create'

generate 'rspec:install'

git :init
git add: '.'
git commit: '-a -m "Initial structure (via GD Bootstrap)"'

if yes?('Link to pow? (y/n)')
  run 'ln -s `pwd` ~/.pow'
end