# Rails Bootstrap

This is a quick and dirty way of booting up a fresh rails app. All you need to know is where you want your Rails app installing.

## The magical one-liner

    ruby -e "$(curl -fsSL https://bitbucket.org/gamaroffdigital/rails-bootstrap/raw/master/bootstrap.rb)" <PATH_TO_APP>

Let's say we want to install it into ~/code/my-app:

    ruby -e "$(curl -fsSL https://bitbucket.org/gamaroffdigital/rails-bootstrap/raw/master/bootstrap.rb)" ~/code/my-app

Boom. Done.