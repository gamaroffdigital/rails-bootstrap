#!/usr/bin/env ruby
require 'fileutils'

location = ARGV[0]

if location.nil? || location == '' 
  abort "Please let us know where you're going to want your Rails install."
end

if RUBY_VERSION.gsub('.', '').to_i < 200
  abort "Your current Ruby version (#{RUBY_VERSION} is out of date. Please upgrade to Ruby >= 2.0.0."
end

latest_rails = `curl -fsSL https://raw.github.com/rails/rails/4-0-stable/RAILS_VERSION`.strip

begin
  gem 'rails', "~> #{latest_rails}"
rescue Gem::LoadError
  abort "Looks like the latest version of Rails isn't installed. Maybe you should run \`gem install rails --version='#{latest_rails}' --no-rdoc --no-ri\`?"
end

FileUtils.mkdir_p(location)

Dir.chdir(location)

system 'rails new . -m https://bitbucket.org/gamaroffdigital/rails-bootstrap/raw/master/rails_template.rb -f --database=postgresql'
